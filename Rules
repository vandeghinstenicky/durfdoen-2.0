#!/usr/bin/env ruby

preprocess do
  # Fetch a list of theme names from all items and construct items from them
  themas = @items.map { |i| i[:themas] }.flatten.uniq.compact
  themas.each do |thema|
    content = ''
    attributes = {
        identifier: thema,
        naam: thema.downcase
    }
    identifier = "/themas/#{thema}"

    @items.create(content, attributes, identifier)
  end
end

compile '/{verenigingen,projecten,konventen}/*.md' do
  filter :kramdown
  layout '/detail.*'
  layout '/default.*'
  layout '/base.*'
  write ext: 'html'
end

compile '/themas/*' do
  layout '/thema.*'
  layout '/default.*'
  layout '/base.*'
  write ext: 'html'
end

compile '/homepage.md' do
  filter :kramdown
  layout '/homepage.*'
  layout '/default.*'
  layout '/base.*'
  write '/index.html'
end

compile '/faq/*' do
  filter :kramdown
end

compile '/verenigingen.md' do
  filter :kramdown
  layout '/verenigingen.*'
  layout '/default.*'
  layout '/base.*'
  write '/verenigingen/index.html'
end

compile '/quiz.md' do
  filter :kramdown
  layout '/quiz.*'
  layout '/default.*'
  layout '/base.*'
  write '/quiz/index.html'
end

compile '/search.html.erb' do
  filter :erb
  layout '/default.*'
  layout '/base.*'
  write ext: 'html'
end

compile '/*.md' do
  filter :kramdown
  layout '/default.*'
  layout '/base.*'
  write ext: 'html'
end

compile '/stylesheets/*.{sass,scss}' do
  syntax = @item.identifier.ext.to_sym
  filter :sass, syntax: syntax, style: :compact
  write ext: 'css'
end

compile '/*.erb' do
  filter :erb
  layout '/default.*'
  layout '/base.*'
  write item.identifier.without_ext + '/index.html'
end

layout '/**/*', :erb

# Catch-all
compile '/**/*' do
  write item.identifier.to_s
end
