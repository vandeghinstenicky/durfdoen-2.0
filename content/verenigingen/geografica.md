---
titel: Geografica
id: geografica
naam: Geografica
verkorte_naam: Geografica
konvent: fk
contact: info@geografica.be
website: http://www.geografica.be
themas:
  -  faculteit
---

Geografica is de studentenkring voor studenten die Geografie & Geomatica studeren aan de UGent. Elk jaar staat een enthousiast Praesidium klaar om de studenten te helpen en tal van activiteiten te organiseren. Geografica heeft een lokaal in de S8 op de Sterre, het Geokot. Dit kot wordt gedeeld met Geologica, onze collega’s van de geologie. Ook in de overpoort hebben we onze stek: café Salto. Hier staat Bertje altijd klaar om de dorstigen te laven.
