---
titel: Vlaamse Diergeneeskundige Kring
id: vdk
naam: Vlaamse Diergeneeskundige Kring
verkorte_naam: Vlaamse Diergeneeskundige Kring
konvent: fk
website: http://www.vlaamsdiergeneeskundigekring.be/
contact: VDK@fkserv.ugent.be
themas:
  -  faculteit
---
De Vlaamse Dierengeneeskundige Kring is de kring voor, je raadt het nooit, studenten Dierengeneeskunde. Doordat hun campus zo ver buiten Gent ligt, is er een hechte band tussen die studenten, een soort dierengeneeskundige familie. Naast hun super activiteiten organiseren ze elk jaar de EXPOVET, een vakbeurs voor dierenartsen in de Flanders Expo.
