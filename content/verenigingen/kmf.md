---
titel: Kring Moraal en Filosofie
id: kmf
naam: Kring Moraal en Filosofie
verkorte_naam: Kring Moraal en Filosofie
konvent: fk
contact: kmf.presidium@gmail.com
website: https://kringmoraalenfilosofie.wordpress.com/
themas:
  -  faculteit
---

De Kring Moraal en Filosofie is de faculteitskring van twee opleidingen aan de Universiteit Gent: wijsbegeerte en moraalwetenschappen. De KMF zorgt elk jaar opnieuw voor een gevarieerd aanbod aan activiteiten gaande van (drank)sport tot cultuuruitstappen. U vindt ons vaak terug aan de kikkertafel of toog van menig Gents café.
