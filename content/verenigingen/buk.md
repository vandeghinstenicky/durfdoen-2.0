---
titel: Brugse Universitaire Kring
id: buk
naam: Brugse Universitaire Kring
verkorte_naam: Brugse Universitaire Kring
konvent: sk
website: https://www.buk-leeft.be/
social:
  - platform: facebook
    link: https://www.facebook.com/BrugseKring/
themas:
  - streek
postcodes:
  - 8000
  - 8020
  - 8200
  - 8210
  - 8211
  - 8300
  - 8301
  - 8310
  - 8340
  - 8370
  - 8377
  - 8380
  - 8490
  - 8730
showcase:
  - photo: /assets/logos/BUKA.jpg
  - photo: /assets/logos/BUKB.jpg
  - photo: /assets/logos/BUKC.jpg
---

De Brugse Universitaire Kring is een studentenvereniging voor en door Brugse Studenten. Al 82 jaar zijn wij er om Bruggelingen elkaar (beter) te leren kennen. We doen dit op een gezapige manier, met een cantus hier, een uitstap daar en vooral regelmatig een avond op café. Vanaf midden september vind je ons elke dinsdag in de Luxx, in het midden van de Overpoort.
