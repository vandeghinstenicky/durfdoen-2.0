---
titel: Capitant Gent
id: capitant
naam: Capitant Gent
verkorte_naam: Capitant Gent
konvent: wvk
contact: info@capitant.be
website: https://capitant.be/
themas:
  - economie en development
showcase:
  - photo: /assets/logos/CapitantA.jpg
  - photo: /assets/logos/CapitantB.jpg
  - photo: /assets/logos/CapitantC.jpg
---

We organise various events about financial markets that include debates, workshops, guest lectures, professional networking events and so much more! On top of all this, we go on international finance trips together.
