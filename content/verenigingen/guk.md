---
titel: Gents Universitair Koor
id: guk
naam: Gents Universitair Koor
verkorte_naam: Gents Universitair Koor
konvent: kultk
website: http://gentsuniversitairkoor.be/
social: 
  - platform: facebook
    link: https://www.facebook.com/gentsuniversitairkoor/
  - platform: instagram
    link: https://www.instagram.com/gentsuniversitairkoor/
themas:
  -  cultuur
showcase:
  - photo: /assets/logos/GUKA.jpg
  - photo: /assets/logos/GUKB.jpg
  - photo: /assets/logos/GUKC.jpg
---

Het Gents Universitair Koor (GUK) is een jong vierstemmig koor bestaande uit (oud-)(doctoraat-)studenten aan het Gents hoger onderwijs. We wagen ons aan een breed repertoire, variërend van polyfonie, over barok en romantische werken, tot 20ste-eeuwse koorwerken met de nadruk op hedendaags-klassieke en Belgische koormuziek. Elk semester geven we één of meerdere concerten. Hiervoor repeteren we iedere dinsdagavond in de therminal. Naast semesteriële concerten, luisteren we ook proclamaties en andere evenementen op. Als afsluiter van het academiejaar trekken we naar het buitenland op concertreis. Reeds bezochte landen zijn Oostenrijk, Spanje, Italië en Polen. Komend academiejaar wordt onze bestemming Amerika. Naast onze jaarlijkse buitenlandse concertreis, staat er ook elk semester een weekend gepland. Dit weekend is in de eerste plaats een repetitieweekend, waar de laatste hand wordt gelegd aan het repertoire. Daarnaast is er ook tijd voor plezier tijdens de ontspannende middagpauzes en traditionele avondactiviteiten. Tijdens het zingen worden ook hechte vriendschappen gesmeed, die worden versterkt tijdens cafébezoekjes, GUKcantussen, cocktailparty’s, …
