---
titel: Chemica
id: chemica
naam: Chemica
verkorte_naam: Chemica
konvent: fk
contact: chemica.praeses@gmail.com
website: https://chemica.fkgent.be/
social:
  - platform: instagram
    link: https://www.instagram.com/chemica_gent/
  - platform: facebook
    link: https://www.facebook.com/ChemicaGent/
themas:
  -  faculteit
---

Chemica is dé faculteitskring voor alle studenten Chemie en Biochemie & Biotechnologie aan de UGent. Deze studentenvereniging heeft als voornaamste doel het studentenleven aangenamer te maken adhv. leuke activiteiten en studiehulp. Iedereen met een lidkaart van Chemica kan meteen meegenieten van onze voordelen!
Wekelijks staat een gemotiveerd team, beter bekend als “het praesidium”, voor jullie klaar. Zij voorzien jullie van cursussen en labomateriaal aan de laagste prijzen. Ook voor unieke cultuuractiviteiten en (on)vergetelijke feestjes moet je bij ons zijn! Sportievelingen kunnen zich uitleven in het interfacultair kampioenschap, de zwemmarathon, 12-urenloop, etc. Wie kiest voor de ultieme studentenervaring kan zich steeds laten dopen. Zo krijg je ook toegang tot de betere cantussen van Gent!
Kortom, Chemica heeft voor iedere student wat te bieden.
