---
titel: Slavia
id: slavia
naam: Slavia
verkorte_naam: Slavia
konvent: fk
contact: Slavia.gent@gmail.com
themas:
  -  faculteit
---
Slavia is de studentenvereniging van en voor de studenten Oost-Europese Talen en Culturen aan de Universiteit Gent. Samen met de vakgroep proberen wij de studenten een rijk aanbod van allerhande activiteiten aan te bieden en onze passie voor het Oostblok met hen te delen. Zo organiseren we allerhande feestjes, zoals onze jaarlijkse café poushkine, waarop iedereen kennis kan maken met de drankcultuur uit Rusland en Oost-Europa, een avondje waarop wodka, rakija en zubrovka rijkelijk vloeit! Verder hebben we onze Oost-Europareis, het galabal en nog andere studentikoze activiteiten. Twee keer per semester brengen wij ook ‘de Slaaf’ uit, een boekje voor en door studenten, met anekdotes en verhalen van afgelopen activiteiten.
