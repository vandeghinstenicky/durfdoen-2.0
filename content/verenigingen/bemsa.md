---
titel: BeMSA
id: bemsa
naam: BeMSA
verkorte_naam: BeMSA
konvent: wvk
contact: info@bemsa-gent.be
website: https://bemsa-gent.be/
social:
  - platform: faceboook
    link: https://www.facebook.com/BeMSAGent
  - platform: instagram
    link: https://www.instagram.com/bemsa_gent/
themas:
  -  wetenschap en techniek
showcase:
  - photo: /assets/logos/bemsaA.jpg
  - photo: /assets/logos/BemsaB.jpg
  - photo: /assets/logos/BemsaC.jpg
---

We zijn de Gentse afdeling van BeMSA, de “Belgian Medical Students Association”, een Belgische organisatie voor en door studenten uit de medische sector.
Onze missie is studenten in de gezondheidszorg verenigen en versterken, door hen uit te rusten met de kennis, vaardigheden en waarden om competente, cultureel bewuste gezondheidswerkers en leiders in Global Health te vormen, voor gelijke toegang tot gezondheidszorg en het bereiken van gezondheid voor iedereen. We werken samen aan een platform voor maatschappelijk engagement en kwalitatieve internationale mogelijkheden voor studenten in de gezondheidssector in Gent.
