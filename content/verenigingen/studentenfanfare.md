---
titel: Studentenfanfare
id: studentenfanfare
naam: Studentenfanfare
verkorte_naam: Studentenfanfare
konvent: kultk
contact: info@sfghendt.be
website: http://www.sfghendt.be/
social:
  - platform: facebook
    link: https://www.facebook.com/StudentenfanfareGhendt/
themas:
  -  cultuur
showcase:
  - photo: /assets/logos/StufaA.jpg
  - photo: /assets/logos/StufaB.jpg
  - photo: /assets/logos/StufaC.jpg
---

Studentenfanfare Ghendt is de enige studentenclub binnen het Kultureel Konvent. De hoofdbezigheid van de studentenfanfare is het opluisteren van allerlei festiviteiten gaande van cantussen tot galabals tot zelfs voetbalwedstrijden. Naast al dat optreden amuseren de leden van Studentenfanfare Ghendt zich al eens bij het drinken van menig pils in hun stamcafé “De Salamander” of, zoals dat hoort bij een studentenclub, op hun eigen cantussen. Muzikanten die eigenlijk wel wat meer willen dan enkel achter een pupiter zitten en die graag écht plezier maken, vinden hier zeker hun goesting.
