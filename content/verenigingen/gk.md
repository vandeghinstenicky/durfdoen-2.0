---
titel: Groene Kring
id: gk
naam: Groene Kring
verkorte_naam: Groene Kring
konvent: wvk
social:
  - platform: facebook
    link: https://www.facebook.com/GroeneKring-BIOT-901093239976866/
themas:
  - duurzaamheid en groen
---

Wij zijn een vereniging voor studenten in Gent die interesse hebben in de land- en tuinbouwsector.
Wij organiseren hoofdzakelijk bedrijfsbezoeken en lezingen die betrekking hebben tot de land- en tuinbouw. Daarnaast organiseren wij ook nog ontspannende activiteiten zoals een quiz of teambuilding.
