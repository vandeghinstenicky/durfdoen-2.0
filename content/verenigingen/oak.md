---
naam: Oosterse Afrikaanse Kring
id: oak
verkorte_naam: Oosterse Afrikaanse Kring
konvent: fk
contact: oak.blandijn@gmail.com
website: https://oak.fkgent.be/
social:
  - platform: facebook
    link: https://www.facebook.com/Oosterse-Afrikaanse-Kring-1378005869118376/
  - platform: instagram
    link: https://www.instagram.com/oosterse_afrikaanse_kring/
  - platform: twitter
    link: https://twitter.com/OAKGent
themas:
  - faculteit
---

De Oosterse Afrikaanse Kring, kortweg de OAK, is de studentenkring voor leerlingen van de richtingen sinologie, indologie, arabistiek en afrikanistiek. Naast het organiseren van de boekenverkoop en het voorzien van studietips waar nodig, verzorgt
de OAK een gevarieerd aanbod aan activiteiten, geheel in lijn met de vele richtingen die zij vertegenwoordigt - denk aan onze befaamde Oosterse thee-avonden, maar denk ook aan karate-workshops, Indisch gaan eten, noem maar op.
De OAK is een kleine, hechte kring die haar studenten zo goed mogelijk wil bijstaan en openstaat voor een babbel en een pintje.
Kom dus eens af!
