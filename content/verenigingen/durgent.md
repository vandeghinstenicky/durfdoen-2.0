---
naam: D'URGENT
id: durgent
verkorte_naam: D'URGENT
titel: D'URGENT
konvent: wvk
website: https://www.greencommunity.be/durgent
social:
  - platform: youtube
  - platform: facebook
    link: https://www.facebook.com/durgentduurzaamdoorstudenten/
  - platform: instagram
    link: https://www.instagram.com/_durgent/
themas:
  - duurzaamheid en groen
showcase:
  - photo: /assets/logos/durgentA.jpg
  - photo: /assets/logos/durgentB.jpg
  - photo: /assets/logos/durgentC.jpg
---

D'URGENT is een studentenbeweging die werkt rond duurzaamheid. We vertrekken vanuit het idee dat studenten een grote impact kunnen hebben op hun leefomgeving.
Wij staan open voor eenieder die goesting heeft om de zaken aan te pakken, elk op zijn eigen interessegebied, elk op zijn eigen tempo. Hoe veel tijd en werk je in de organisatie steekt is volledig je eigen keuze, en ongeacht welke keuze je maakt, je steentje zal je zeker kunnen bijdragen!
