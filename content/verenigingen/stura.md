---
naam: StuRa
verkorte_naam: StuRa
titel: StudentenRaad Politieke en Sociale Wetenschappen
id: stura
naam: StudentenRaad Politieke en Sociale Wetenschappen
konvent: gsr
website: https://stura.ugent.be/
social:
  - platform: facebook
    link: https://www.facebook.com/StuRaPSW
themas:
  -  engagement
---

StuRa is de studentenraad voor en door studenten van de faculteit Politieke en Sociale Wetenschappen aan de Universiteit Gent. Onze voornaamste opdracht is de belangen van de studenten Politieke Wetenschappen, Sociologie en Communicatiewetenschappen het hele jaar door te behartigen. Concreet wil dit zeggen dat we zetelen in allerlei vergaderingen met professoren en universiteitsmedewerkers.
Op deze manier krijgen wij inspraak in het beleid van de faculteit en de kwaliteit van de opleiding. Daarnaast zorgen wij er ook voor dat onderwijsgerelateerde klachten bij onze studenten (bv. over opleidingsonderdelen, moeilijkheden met professoren of examenroosters) efficiënt en effectief worden aangepakt.
StuRa komt om de twee weken samen om debat te voeren over de "hot topics" die studenten aanbelangen, we nemen hierover standpunten in en houden studenten op de hoogte van het reilen en zeilen aan onze universiteit. Onze vergaderingen gaan over de serieuzere kwesties die we met de nodige ernst bespreken, maar spelen zich af in een amicale sfeer waarbij we streven naar een hartelijk groepsgevoel waar iedereen zich in thuis kan voelen. Alle studenten zijn steeds welkom op onze vergaderingen. Kortom, naar onze Algemene Vergaderingen komen is ideaal om iets bij te leren over de faculteit en om nieuwe mensen te leren kennen!
