---
titel: Vlaamse Economische Kring
id: vek
naam: Vlaamse Economische Kring
verkorte_naam: Vlaamse Economische Kring
konvent: fk
website: http://www.vek.be/
contact: praeses@vek.be
themas:
  -  faculteit
---
Als sinds 1923 is de Vlaamse Economische Kring de studentenvereniging van de hele Faculteit Economie en Bedrijfskunde. Onze kalender staat bol van studentikoze activiteiten: van bierbowlen tot paintballen, van citytrips tot skireizen en van kroegentochten tot ons befaamd galabal.
Het centrum van onze vereniging is de Yucca, het enige studentencafé in Gent waar je aan zeer studentikoze prijzen een dik feestje kan bouwen of gewoon kan genieten van ons geweldig terras!
Naast deze dagelijkse activiteiten verzorgen wij ook de boekenverkoop, organiseren wij het tweedaags fantastisch straatvoetbaltornooi Student Street Soccer, en hebben wij een professionele recruitmentwerking die onder andere zorgt voor een enorm succesvolle Career Day.
