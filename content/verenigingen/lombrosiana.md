---
titel: Lombrosiana
id: lombrosiana
naam: Lombrosiana
verkorte_naam: Lombrosiana
konvent: fk
contact: praeses@lombrosiana.be
website: http://www.lombrosiana.be
social:
  - platform: instagram
themas:
  -  faculteit
---

Lombrosiana is de faculteitskring van de opleiding criminologie. Ze bieden een gevarieerd aanbod van fuiven, cultuur, sport, studie en dit alles aan studentikoze prijzen. Op elk vlak is Lombrosiana wel vertegenwoordigd onder het motto ‘Lombrosiana boven en dat voor altijd, wij willen elkander voor nimmer meer kwijt.'
