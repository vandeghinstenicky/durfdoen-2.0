---
naam: Stuveco
verkorte_naam: Stuveco
titel: Studentenraad Faculteit Economie en Bedrijfskunde
id: stuveco
naam: Studentenraad Faculteit Economie en Bedrijfskunde
konvent: gsr
website: https://stuveco.be
themas:
  -  engagement
showcase:
  - photo: /assets/logos/StuvecoA.jpg
  - photo: /assets/logos/StuvecoB.jpg
  - photo: /assets/logos/StuvecoC.jpg
---

Stuveco is de studentenraad van de faculteit Economie en Bedrijfskunde. Onze studentenvertegenwoordigers zitten in verschillende raden en commissies waar we samen met professoren en assistenten de faculteit mee besturen. We waken er over jouw belangen en kaarten er problemen aan. Zo ijveren we voor goede lesgevers, eerlijke examens, voldoende studentenvoorzieningen, enz.
