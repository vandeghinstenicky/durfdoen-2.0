---
titel: Jongsocialisten StuGent
id: socialisten
naam: Jongsocialisten StuGent
verkorte_naam: Jongsocialisten StuGent
konvent: pfk
contact: jongsocialisten_stugent@gmail.com
social:
  - platform: facebook
    link: https://www.facebook.com/js.stugent/
themas:
  -  politiek en filosofisch
---

Het studentennetwerk "Jongsocialisten Student" is opgericht om gezamenlijke actie tussen de verschillende studentenafdelingen op te zetten. Het is een ontmoetingsplek waar studenten uit de verschillende steden elkaar beter kunnen leren kennen.
Het netwerk brengt studenten met elkaar in contact, denkt na over studententhema's, werkt gerichte acties uit en stuurt haar leden op avontuur.
Voor meer info kan je bij de nationale coördinator of de afdelingen terecht.
