---
titel: Moeder lies
id: lies
naam: Moeder lies
verkorte_naam: Moeder lies
konvent: fk
contact: praeses@moederlies.be
website: http://www.moederlies.be
social:
  - platform: facebook
    link: https://www.facebook.com/moederliesofficial/
  - platform: instagram
    link: https://www.instagram.com/moederlies/
  - platform: youtube
    link: https://www.youtube.com/channel/UCE_KfYgxZN6NmyaGPMyLmXA
themas:
  -  faculteit
---

Met trots stellen wij onze studentenkring Moeder Lies voor. Moeder lies
is sinds 1995 de officiële studentenkring van Handelswetenschappen en
Bestuurskunde & Publiek management te Gent. Wij vertegenwoordigen en
ondersteunen de studenten aan de Faculteit Economie en Bedrijfskunde op
talloze manieren.

Zo organiseren wij diverse activiteiten die zowel de typische
studentikoze tradities waarborgen als zaken om jullie te helpen in
jullie studies. Wij staan namelijk in voor de boekenverkoop doorheen
jullie volledige opleiding en zorgen voor de nodige ontspanning via
sportactiviteiten, culturele evenementen, knotsgekke feestjes en nog zo
veel meer. Je kan ons steeds vinden in ons stamcafé Delirium Tremens
waar we de boel op stelten zetten!
Ondanks dat we één van de grootste studentenverenigingen zijn van Gent,
gaat onze familiale omgang met elkaar er zeker niet aan verloren.
