---
titel: Home Fabiola
id: fabiola
naam: Home Fabiola
verkorte_naam: Home Fabiola
konvent: hk
social:
  - platform: twitter
    link: http://twitter.com/Home_Fabiola
  - platform: facebook
    link: http://www.facebook.com/groups/229033760483915/
themas:
  -  home
showcase:
  - photo: /assets/logos/FabiolaA.jpg
  - photo: /assets/logos/FabiolaB.jpg
  - photo: /assets/logos/FabiolaC.jpg
---

Op maar een paar meter afstand van de befaamde Overpoortstraat, het studentenrestaurant Kantienberg en de Albert Heyn, is Home Fabiola ideaal gelegen voor de student die dagelijkse wandelingen liever tot een minimum beperkt. Het gebouw werd gebouwd in de jaren '60 en telt 224 kamers van 12 m². Verder is de home ook voorzien van een ruime studeerzaal, een solarium met dakterras, en een ontspanningszaal waar je met je kotgenoten naar tv kunt kijken, kunt kickeren, pingpongen, poolen of elke woensdag van een drankje kunt genieten op onze baravond.
Dankzij al die gemeenschappelijke ruimtes is het heel eenvoudig om hier hechte banden te scheppen. Homebewoners voelen zo al snel aan als een tweede familie: iedereen kent elkaar en staat voor elkaar klaar. Heb je een probleem, heb je nood aan een goede babbel of heb je gewoon iemand nodig met wie je kunt uitgaan? Klop maar eens aan bij een ganggenoot en je vindt gegarandeerd iemand die je kan helpen!
Opdat die vriendschappen onder homebewoners gemakkelijker worden gesmeed, is er een homeraad die elke week culturele en sportieve activiteiten organiseert. Zo gaan we weleens een theaterstuk bijwonen, verkennen we Gent met een fietstocht of kroegentocht en werken we ook aan onze conditie met een wekelijkse start-to-run in het park, of start-to-sport bij slecht weer. Maar er zijn ook grotere activiteiten zoals een quiz die elk semester wordt georganiseerd of een kaas-en-wijnavond die tegelijkertijd ook een galabal is.
Één ding is dus zeker: tijd om je te vervelen heb je niet op Home Fabiola!
