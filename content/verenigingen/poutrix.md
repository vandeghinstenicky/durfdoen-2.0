---
titel: Poutrix
id: poutrix
naam: Poutrix
verkorte_naam: Poutrix
konvent: wvk
contact:  info@poutrix.be
website: https://www.poutrix.be/
social:
  - platform: facebook
    link: https://www.facebook.com/Poutrix/
themas:
  - wetenschap en techniek
showcase:
  - photo: /assets/logos/PoutrixA.jpg
  - photo: /assets/logos/PoutrixB.jpg
  - photo: /assets/logos/PoutrixC.jpg
---

Poutrix is de technische studentenvereniging van de burgerlijk ingenieurs bouwkunde aan de Universiteit Gent. Ons doel is een brug te vormen tussen de abstracte, theoretische bouwkunde en de échte bouwwereld. Hiervoor worden werfbezoeken, bedrijfsbezoeken, lezingen en deelnames aan congressen en studiedagen georganiseerd.
