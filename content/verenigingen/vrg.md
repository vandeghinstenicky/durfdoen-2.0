---
titel: Vlaams Rechtsgenootschap
id: vrg
naam: Vlaams Rechtsgenootschap
verkorte_naam: Vlaams Rechtsgenootschap
konvent: fk
website: https://www.vrg-gent.be
contact: praeses@vrg-gent.be
themas:
  -  faculteit
showcase:
  - photo: /assets/logos/VRGA.jpg
  - photo: /assets/logos/VRGB.jpg
  - photo: /assets/logos/VRGC.jpg
---
Het Vlaams Rechtsgenootschap Gent is de grootste Gentse studentenvereniging voor de juristen in opleiding. Wij zorgen doorheen het hele jaar voor tal van activiteiten die het studentenleven de mooiste tijd van het leven maken!

Zo organiseren we elk jaar een OpeningsTD in de Vooruit, waar we met meer dan 1000 confraters de boel op stelten zetten. In het tweede semester pakken we uit met een fantastisch Galabal. Ook de sportievelingen komen aan hun trekken, aangezien we wekelijks aan wedstrijden en toernooien meedoen van meer dan 15 verschillende sporten.
Cultuur mag zeker ook niet missen. Geniet mee van een zelfgeschreven toneelstuk en waaier aan interessante en verrijkende uitstapjes en activiteiten!

Daarnaast helpen we de studenten waar nodig door boeken te verkopen aan de scherpste prijzen. We bieden eveneens tal van tips & tricks, notities, samenvattingen en examenvragen aan die we verzameld hebben in de afgelopen jaren. Ook jobinfoavonden, pleitworkshops, pleitwedstrijden en onze fameuze VRG Jobbeurs (de grootste in België voor juridische beroepen) zijn fantastische evenementen om de student voor te bereiden op het latere werkleven.
