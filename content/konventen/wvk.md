---
titel: Werkgroepen & Verenigingen Konvent
id: wvk
naam: Werkgroepen & Verenigingen Konvent
verkorte_naam: Werkgroepen & Verenigingen Konvent
konvent: wvk
website: https://wvk.ugent.be/
contact: wvk@student.ugent.be
---

Het WVK groepeert alle maatschappelijk-sociaal geïnspireerde studentenverenigingen en alle wetenschappelijke werkgroepen. Als konvent organiseert het WVK zelf (in tegenstelling tot sommige andere konventen) geen eigen activiteiten, maar richt het zich volledig op het samenbrengen en verdedigen van de belangen van de maatschappelijke verenigingen en wetenschappelijke werkgroepen aan de Universiteit Gent.

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwvkcentraal%2Fvideos%2F795092330635099%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden; margin: auto;" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
