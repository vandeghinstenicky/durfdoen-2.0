---
titel: Senioren Konvent
id: sk
naam: Senioren Konvent
verkorte_naam: Senioren Konvent
konvent: sk
themas:
  -  streek
contact: sk@student.ugent.be
website: https://skghendt.be/
socials:
  - platform: twitter
    link: https://twitter.com/SKGhendt
  - platform: facebook
    link: https://www.facebook.com/SeniorenKonventGhendt
---
Het SK is de verzamelplaats voor streekgebonden studentikoze clubs.
